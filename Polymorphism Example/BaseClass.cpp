#include "BaseClass.h"

// Constructor
BaseClass::BaseClass()
{

}

// Destructor
BaseClass::~BaseClass()
{

}

// The function we wish to override
void BaseClass::ShowMessage(string text)
{
	// The base message we will output
	cout << "Base message shown is: " << text << endl;
}