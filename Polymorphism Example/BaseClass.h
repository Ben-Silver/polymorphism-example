#pragma once
#ifndef BASECLASS_H
#define BASECLASS_H

/* Includes */
#include <string>
#include <iostream>
using namespace std;

// Base class
class BaseClass
{
	/* Public Variables/Functions */
	public:
		// Constructor
		BaseClass();

		// Destructor
		~BaseClass();

		// The function we wish to override
		virtual void ShowMessage(string text);
};

#endif // !BASECLASS_H