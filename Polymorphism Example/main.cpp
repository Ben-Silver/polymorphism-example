/* Includes */
#include "ChildClass.h"
#include <string>
#include <iostream>
using namespace std;

// Main function
int main(void)
{
	// New child class object using a base class reference
	BaseClass *bp = new ChildClass;

	// ShowMessage function call with the intended message
	bp->ShowMessage("Blah");		// run-time polymorphism

	// Pause so we can see what's happening
	system("pause");

	// Nothing to return
	return 0;
}