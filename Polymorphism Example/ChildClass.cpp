#include "ChildClass.h"

// Constructor
ChildClass::ChildClass()
{

}

// Destructor
ChildClass::~ChildClass()
{

}

void ChildClass::ShowMessage(string text)
{
	// The child message we will output
	cout << "Child message shown is: " << text << endl;
}