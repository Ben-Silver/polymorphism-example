#pragma once
#ifndef CHILDCLASS_H
#define CHILDCLASS_H

/* Includes */
#include "BaseClass.h"

// Derived/child class
class ChildClass : public BaseClass
{
	/* Public Variables/Functions */
public:
	// Constructor
	ChildClass();

	// Destructor
	~ChildClass();
	
	// The function we are redefining
	void ShowMessage(string text);
};

#endif // !CHILDCLASS_H